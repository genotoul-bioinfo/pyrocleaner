#
# Pyrocleaner wrapper for galaxy
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.2'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

import optparse, os, shutil, subprocess, sys, tempfile, re, string
    
if __name__ == "__main__":
    
    parser = optparse.OptionParser()
    
    parser.add_option( '-s', '--options',      dest='options',      help='The pyrocleaner options' )
    parser.add_option( '-l', '--log',          dest='log',          help='Path to the pyrocleaner log' )
    parser.add_option( '-f', '--format',       dest='format',       help='The input file format (sff|fastq)' )
    parser.add_option( '-o', '--output',       dest='output',       help='The output file path' )
    parser.add_option( '-a', '--out-pe-qual',  dest='out_pe_qual',  help='The output pairends qual file path' )
    parser.add_option( '-b', '--out-pe-fasta', dest='out_pe_fasta', help='The output pairends fasta file path' )
    parser.add_option( '-d', '--out-dir',      dest='out_dir',      help='The output dir where to process the data' )
    
    (options, args) = parser.parse_args()
    
    buffsize = 1048576
    tmp_dir = tempfile.mkdtemp()
    
    try:
    
        if options.format == "sff": format = "sff"
        else : format = "fastq"
        
        tmp_stderr_name = tempfile.NamedTemporaryFile( dir=tmp_dir,suffix='.err' ).name
        tmp_stderr = open( tmp_stderr_name, 'wb' )
        tmp_stdout_name = tempfile.NamedTemporaryFile( dir=tmp_dir,suffix='.out' ).name
        tmp_stdout = open( tmp_stdout_name, 'wb' )
        cmd = 'pyrocleaner --format %s %s --out %s' % (format, options.options, options.out_dir)
        proc = subprocess.Popen( args=cmd, shell=True, cwd=tmp_dir, stderr=tmp_stderr.fileno(), stdout=tmp_stdout.fileno() )
        returncode = proc.wait()
        tmp_stderr.close()
        # get stderr, allowing for case where it's very large
        tmp_stderr = open( tmp_stderr_name, 'rb' )
        stderr = ''
        try:
            while True:
                stderr += tmp_stderr.read( buffsize )
                if not stderr or len( stderr ) % buffsize != 0:
                    break
        except OverflowError:
            pass
        tmp_stderr.close()
        if returncode != 0:
            raise Exception, stderr
        
        # Move the log file to the right place
        shutil.move(os.path.join(options.out_dir, "pyrocleaner.log"), options.log)
        
        for file in os.listdir(options.out_dir):
            # If the file has the input file format and is not a shotgun file
            if file.endswith(format):
                shutil.move(os.path.join(options.out_dir, file), options.output)
            # If it's a quality file from shotgun
            if file.endswith(".qual") :
                shutil.move(os.path.join(options.out_dir, file), options.out_pe_qual)
            # If it's a fasta file from shotgun
            elif file.endswith(".fasta") :
                shutil.move(os.path.join(options.out_dir, file), options.out_pe_fasta)

    except Exception:
        raise Exception, 'Error executing pyrocleaner.'
