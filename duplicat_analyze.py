#!/usr/local/bioinfo/bin/python2.5

#
# duplicat_analyze 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

import os, math, datetime, sys, string, gzip, glob
from Bio import SeqIO
from optparse import *
from igraph import *

def version_string ():
    """
    Return the get_duplicat_analyze version
    """
    return "duplicat_analyze " + __version__


def get_duplicat_analyze (seqs, options):
    """
    Filter input seqs by duplicat, if sequences are too similar keep only one to represent the cluster
      @param seqs    : table of seqs to filter   
      @param options : the options asked by the user
    """
    
    megablast_input = os.path.join(options.output, "megablast_input.fasta")
    log.write("## formatdb the fasta file (" + str(datetime.datetime.now()) + ")\n")
    
    # First write down seqs id doesn't exists
    if not os.path.isfile(megablast_input):
        fasta = open(megablast_input, "w")
        SeqIO.write(seqs, fasta, "fasta")
        fasta.close()
    
    if not os.path.isfile(megablast_input+".res"):
        # Then formatdb the fasta file (no formatdb utils in Biopython)
        cmd = "formatdb -i %s -p F" % megablast_input
        os.system(cmd)    
        log.write("## megablast (" + str(datetime.datetime.now()) + ")\n")
        # Megablast the fasta file versus itself
        cmd = "megablast -d " + megablast_input + " -i " + megablast_input + " -p 98 -a " + str(options.nb_cpus) + " -s 100 -D 3 | grep -v '^#' | perl -lne 'chomp; split; if ($_[0] ne $_[1]) { if (($_[6] == 1 ) && ($_[8] == 1 || $_[9] == 1)) { print $_; }}' > " + megablast_input + ".res"
        os.system(cmd)    
    
    gseqs = {}
    for reads_record in seqs :
        gseqs[reads_record.id] = len(reads_record)
    
    vertices = []
    for reads_record in seqs :
        vertices.append({'name': reads_record.id})
    
    clusters_final_stats = []
    max_nb_cluster = 0
    cluster_window = []
    
    for i, window in enumerate(range(options.begin, options.end, options.step)):
        duplication_limit = window
        # Connects reads from hits starting at 1 and with ends closed to each others
        # Let's get the reads length with the fasta file and creates the graph
        log.write("## Computing window " + str(i) + " using duplication limit = " + str(duplication_limit) + " (" + str(datetime.datetime.now()) + ")\n")
        edges = []
        for read in open(megablast_input + ".res", 'rU').readlines() :
            parts = read.rstrip().split()
            len1 = gseqs[parts[0]]
            len2 = gseqs[parts[1]]
            if math.fabs(len1-len2) < duplication_limit:
                if int(parts[7]) > (len1 - duplication_limit) and int(parts[9]) > len2 - duplication_limit:
                    # This alignments are realy similar -> may be the same -> link them into the graph
                    edges.append({'source': parts[0], 'target': parts[1]})
        
        # Then get connected components and extract one of them as cluster leader
        gr = Graph.DictList(vertices, edges)
        connex = gr.clusters().membership
        
        clusters = {}
        for i, vertex in enumerate(vertices):
            cluster_id = connex[i]
            try:
                clusters[cluster_id].append(seqs[i])
            except:
                clusters[cluster_id] = []
                clusters[cluster_id].append(seqs[i])
    
        # Make some stats
        clusters_stats = {}
        for cluster_id in clusters:
            try :
                clusters_stats[len(clusters[cluster_id])] += 1
            except:
                clusters_stats[len(clusters[cluster_id])] = 1
        
        clusters_final_stats.append(clusters_stats)
        cluster_window.append(duplication_limit)
        if len(clusters_stats) > max_nb_cluster:
            max_nb_cluster = len(clusters_stats)
        
    if max_nb_cluster > options.max_nb_clusters:
        max_nb_cluster = options.max_nb_clusters
    
    # Write down a summary of what has been done
    log_header = "## header : "
    for i in range(max_nb_cluster):
        log_header += str(i) + "\t"
    log.write(log_header + "\n")
    
    for i, limit in enumerate(cluster_window):
        log_summary = "## summary (duplication limit=" + str(limit) + ") : "
        indice = 0
        for stat in sorted(clusters_final_stats[i].keys()):
            if indice > max_nb_cluster:
                break
            elif indice > len(clusters_final_stats[i]):
                log_summary += "0\t"
            else :
                log_summary += str(clusters_final_stats[i][stat]) + "\t"
        log.write(log_summary + "\n")


def get_seqs (options):
    """
    Converts input seqs in a BioPython seq table
      @param options : the options asked by the user
    """
    
    # First get fasta or/and qual input files
    if options.format == "sff":
        sff_file = options.input_file
        
        if sff_file.endswith(".gz"):
            '''Gunzip the given file and then remove the file.'''
            r_file = gzip.GzipFile(sff_file, 'r')
            write_file = string.rstrip(sff_file, '.gz')
            w_file = open(write_file, 'w')
            w_file.write(r_file.read())
            w_file.close()
            r_file.close()
            sff_file = write_file
            
        base = os.path.basename(sff_file)
        fasta_file = os.path.join(options.output, base + '.fasta')
        qual_file = os.path.join(options.output, base + '.qual')
        xml_file = os.path.join(options.output, base + '.xml')
        format = "fasta"
        if not os.path.isfile(fasta_file) or not os.path.isfile(qual_file) or not os.path.isfile(xml_file):
            #First extract the sff file to fasta, qual and xml file
            cmd = "sff_extract.py -c -s " + fasta_file + " -q " + qual_file + " -x " + xml_file + " " + sff_file + " >> " + options.log_file
            os.system(cmd)
        else :
            log = open(options.log_file, "a+")
            log.write(fasta_file + ", " + qual_file + ", " + xml_file + " already exist, working on existing files\n")
            log.close()
    elif options.format == "fasta" or options.format == "fastq":
        format = options.format
        fasta_file = options.input_file
    else :
        print "Error : " + options.format + " is not a supported format!"
        sys.exit(1)

    # If the fasta file is gziped
    if fasta_file.endswith(".gz"):
        seqs = []
        for record in SeqIO.parse(gzip.open(fasta_file), format) :
            seqs.append(record)
    else :
        seqs = []
        for record in SeqIO.parse(open(fasta_file), format) :
            seqs.append(record)
    
    # Returns the table of sequences
    return [seqs]


def clean_tmp_files (options):
    """
    Clean the output directory
      @param options : the options asked by the user
    """
    for files in glob.glob(os.path.join(options.output, "*")):
        if not os.path.splitext(files)[0].endswith(".clean") and not files.endswith("log") and files != os.path.join(options.output, options.input_file):
            os.remove(files)
        if files.endswith(".cross_match_input.fasta.log"):
            os.remove(files)


def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def get_duplicat_analyze_depts_error():
    """
    Return the list of dependencies missing to run get_duplicat_analyze
    """
    error = ""
    if which("formatdb") == None:
        error += " - formatdb is missing\n" 
    if which("megablast") == None:
        error += " - megablast is missing\n" 
    if which("sff_extract.py") == None:
        error += " - sff_extract.py\n" 
    if error != "":
        error = "Cannot execute get_duplicat_analyze, following dependencies are missing :\n" + error + "Please install them before to run get_duplicat_analyze!"
    return error
    

if __name__ == "__main__":

    if not get_duplicat_analyze_depts_error():
        parser = OptionParser(usage="Usage: duplicat_analyze.py -i FILE [options]")

        usage = "usage: %prog -i file -o output -f format"
        desc = " TODO \n"\
               " ex : duplicat_analyze.py -i file.sff -o /path/to/output -f sff"
        parser = OptionParser(usage = usage, version = version_string(), description = desc)

        igroup = OptionGroup(parser, "Input files options","")
        igroup.add_option("-i", "--in", dest="input_file",
                          help="The file to clean, can be [sff|fastq|fasta]", metavar="FILE")
        igroup.add_option("-f", "--format", dest="format",
                          help="The format of the input file [sff|fastq|fasta] default is sff", type="string", default="sff")
        parser.add_option_group(igroup)
        
        ogroup = OptionGroup(parser, "Output files options","")
        ogroup.add_option("-o", "--out", dest="output",
                          help="The output folder where to store results")
        ogroup.add_option("-g", "--log", dest="log_file",
                          help="The log file name (default:duplicat_analyze.log)", metavar="FILE", default="duplicat_analyze.log")
        parser.add_option_group(ogroup)
        
        pgroup = OptionGroup(parser, "Processing options","")
        pgroup.add_option("-a", "--acpus", dest="nb_cpus",
                          help="Number of processors to use", type="int", default=1)
        pgroup.add_option("-r", "--recursion", dest="recursion",
                          help="Recursion limit when computing duplicated reads", type="int", default=1000)
        parser.add_option_group(pgroup)
        
        cpgroup = OptionGroup(parser, "duplicat_analyze parameters","")     
        cpgroup.add_option("-b", "--begin", dest="begin",
                          help="The window begin value (default=0)", type="int", default=0)        
        cpgroup.add_option("-e", "--end", dest="end",
                          help="The window end value (default=500)", type="int", default=500)
        cpgroup.add_option("-p", "--step", dest="step",
                          help="The number of window wanted (default=50)", type="int", default=50)    
        cpgroup.add_option("-m", "--max-nb-clusters", dest="max_nb_clusters",
                          help="The maximum number of cluster to return (default=50)", type="int", default=50)  
        parser.add_option_group(cpgroup)        

        (options, args) = parser.parse_args()
        
        sys.setrecursionlimit(options.recursion)
        
        if options.input_file == None or options.output == None and options.format == None:
            parser.print_help()
            sys.exit(1)    
        else:
            global log
            log = open(options.log_file, "w")
            log.write("## Start processing (" + str(datetime.datetime.now()) + ")\n")
            log.close()
            
            # 1 - First get inputs from options
            [seqs] = get_seqs(options)
            
            # 2 - comput graphs
            log = open(options.log_file, "a+")
            get_duplicat_analyze(seqs, options)
            
            log.write("## Ended with code 0 (" + str(datetime.datetime.now()) + ")\n")
            log.close()
            clean_tmp_files(options)
            sys.exit(0)
    else : 
        print get_duplicat_analyze_depts_error()
        sys.exit(1)