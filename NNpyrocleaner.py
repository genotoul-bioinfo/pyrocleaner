#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

from optparse import *
import os, sys, re, math, datetime
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


# The Chi2 table
CHI2_TABLE = {"15": {"0.10" : 22.307, "0.05" : 24.996, "0.01" : 30.578}}


def sort_nns_by_cluster(seq_nns, names_file):
    """
    Sort sequences by cluster and count nns
      @param seq_nns    : {"seq_id": "AA", ...}
      @param names_file : the accession file describing clusters
      @return           : {"cluster": {"AA" : n, ...}, ...}
    """

    clusters = open(options.names_file, 'r')  
    nns_by_cluster = {}
    for i, line in enumerate(clusters.readlines()):
        parts = line.rstrip().split()
        cluster_name = "cluster_" + str(i) + "_" + str(len(parts[1].split(",")))
        nns_by_cluster[cluster_name] = {}
        for seq_id in parts[1].split(","):
            if seq_nns.has_key(seq_id):
                if nns_by_cluster[cluster_name].has_key(seq_nns[seq_id]):
                    nns_by_cluster[cluster_name][seq_nns[seq_id]] += 1
                else :
                    nns_by_cluster[cluster_name][seq_nns[seq_id]] = 1
    clusters.close()
    return nns_by_cluster


def get_seqs_by_cluster(seq_nns, fasta_file, names_file):
    """
    Sort sequences by cluster and count nns
      @param seq_nns    : {"seq_id": "AA", ...}
      @param fasta_file : the fasta file
      @param names_file : the accession file describing clusters
      @return           : {"cluster": {"AA" : [record, ...], ...}, ...}
    """
    # First get all sequences into a python dictionary
    handle = open(fasta_file, "rU")
    seqs = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
    handle.close()
    
    clusters = open(names_file, 'r')  
    seq_by_cluster = {}
    for i, line in enumerate(clusters.readlines()):
        parts = line.rstrip().split()
        cluster_name = "cluster_" + str(i) + "_" + str(len(parts[1].split(",")))
        seq_by_cluster[cluster_name] = {}
        for seq_id in parts[1].split(","):
            if seq_nns.has_key(seq_id):
                if seq_by_cluster[cluster_name].has_key(seq_nns[seq_id]):
                    seq_by_cluster[cluster_name][seq_nns[seq_id]].append(seqs[seq_id])
                else :
                    seq_by_cluster[cluster_name][seq_nns[seq_id]] = [seqs[seq_id]]
    clusters.close()
    return seq_by_cluster


def get_nns(options):
    """
    Seek for NNs throught all sequences in options.fasta_file
      @param options : user options
      @return        : [{"seq_id": "AA", ...}, ["AA", ...]]
    """
    # Cross_match reverse matches pattern
    rev_regex = re.compile("(\s+)?(\d+)\s+(\S+)\s+\S+\s+\S+\s+(\S+)\s+(\S+)\s+(\S+)\s+\S+\s+C\s+(\S+)\s+\S+\s+(\S+)\s+(\S+)")
    # Cross_match forward matches pattern
    fwd_regex = re.compile("(\s+)?(\d+)\s+(\S+)\s+\S+\s+\S+\s+(\S+)\s+(\S+)\s+(\S+)\s+\S+\s+(\S+)\s+(\S+)\s+(\S+)\s+\S+")

    oligo_file = open(os.path.join(options.output_folder, os.path.basename(options.fasta_file) + ".oligo.fna"), "w")
    SeqIO.write([SeqRecord(Seq(options.forward), id = 'fwd'), SeqRecord(Seq(options.reverse), id = 'rvs')], oligo_file, "fasta")        
    oligo_file.close()
    
    cmd = "cross_match " + options.fasta_file + " " + os.path.join(options.output_folder, os.path.basename(options.fasta_file) + ".oligo.fna") + " -minmatch 4 -minscore 6 -penalty -1 -gap_init -1 -gap_ext -1 -ins_gap_ext -1 -del_gap_ext -1 -raw > " + os.path.join(options.output_folder, os.path.basename(options.fasta_file)) + ".cross_match.res"
    os.system(cmd)
    
    reads = {}
    for line in open(os.path.join(options.output_folder, os.path.basename(options.fasta_file) + ".cross_match.res"), 'r'):
        rm = rev_regex.match(line)
        fm = fwd_regex.match(line)
        strand = ""
        save = False
        if rm != None: # If it's a reverse matches
            try :
                (score, percentMis, primary_match, startFirstMatch, endFirstMatch, secondary_match, endSecondMatch, startSecondMatch)=(int(rm.group(2)), float(rm.group(3)), rm.group(4), int(rm.group(5)), int(rm.group(6)), rm.group(7), int(rm.group(8)), int(rm.group(9)))
                save = True
                strand = 'rvs'
            except :
                pass
        elif fm != None: # If it's a forward matches
            try :
                (score, percentMis, primary_match, startFirstMatch, endFirstMatch, secondary_match, startSecondMatch, endSecondMatch)=(int(fm.group(2)), float(fm.group(3)), fm.group(4), int(fm.group(5)), int(fm.group(6)), fm.group(7), int(fm.group(8)), int(fm.group(9)))
                save = True
                strand = 'fwd'
            except :
                pass
            
        if line.startswith("Discrepancy summary:"): # This is the end of the section
            break
        
        if save :
            try :
                reads[primary_match].append([strand, secondary_match, startFirstMatch, endFirstMatch, startSecondMatch, endSecondMatch, score])
            except:
                reads[primary_match] = [[strand, secondary_match, startFirstMatch, endFirstMatch, startSecondMatch, endSecondMatch, score]]
                  
    seq_nns = {}
    nns = []
    for record in SeqIO.parse(open(options.fasta_file), "fasta") :
        start = 0
        stop = len(record.seq)
        fwd_present = False
        rvs_present = False
        is_reversed = False
        
        if reads.has_key(record.id) :
            for match in reads[record.id]:
                if match[0] == 'fwd' and match[1] == 'fwd' and match[6] >= options.fwd_minmatch :
                    start = match[2] - 1
                    fwd_present = True
                elif match[0] == 'fwd' and match[1] == 'rvs' and match[6] >= options.rvrs_minmatch:
                    start = match[2] - 1
                    rvs_present = True
                    is_reversed = True
                elif match[0] == 'rvs' and match[1] == 'rvs' and match[6] >= options.rvrs_minmatch:
                    stop = match[3]
                    rvs_present = True
                elif match[0] == 'rvs' and match[1] == 'fwd' and match[6] >= options.fwd_minmatch:
                    stop = match[3]
                    fwd_present = True
                    is_reversed = True
        
        if options.full_resolution :
            if fwd_present and rvs_present :
                if is_reversed :
                    ns = str(record.seq[stop:stop+options.nb_nns].reverse_complement()+record.seq[start-options.nb_nns:start].reverse_complement())
                else :
                    ns = str(record.seq[start-options.nb_nns:start]+record.seq[stop:stop+options.nb_nns])
                if len(ns) == 2*options.nb_nns and ns.count("N") + ns.count("n") == 0:
                    seq_nns[record.id] = ns
                    if ns not in nns:
                        nns.append(ns)           
        elif options.only_reverse and rvs_present :
            if is_reversed :
                ns = str(record.seq[start-options.nb_nns:start].reverse_complement())
            else :
                ns = str(record.seq[stop:stop+options.nb_nns])
            if len(ns) == options.nb_nns and ns.count("N") + ns.count("n") == 0:
                seq_nns[record.id] = ns
                if ns not in nns:
                    nns.append(ns)   
        elif not options.only_reverse and fwd_present :
            if is_reversed :
                ns = str(record.seq[stop:stop+options.nb_nns].reverse_complement())
            else :
                ns = str(record.seq[start-options.nb_nns:start])
            if len(ns) == options.nb_nns and ns.count("N") + ns.count("n") == 0:
                seq_nns[record.id] = ns
                if ns not in nns:
                    nns.append(ns)   
    
    #Clean up temp files
    os.remove(os.path.join(options.output_folder, os.path.basename(options.fasta_file) + ".oligo.fna"))
    os.remove(os.path.join(options.output_folder, os.path.basename(options.fasta_file) + ".cross_match.res"))
    
    return [seq_nns, nns]


def chi2 (nns_by_cluster, nns, nb_nns, p_value):
    """
    Compute a chi2 on nns_by_cluster using the p_value
      @param nns_by_cluster : {"cluster": {"AA" : n, ...}, ...}
      @param nns            : ["AA", ...]
      @param nb_nns         : Number of NNs used
      @param p_value        : the p_value to use for the chi2 test
      @return               : ["cluster_name", "NN_value"] to correct [None, None] if no chi2 error found
    """
    nns_tot = {}
    cluster_tot = {}
    tot = 0
    for cluster in nns_by_cluster.keys() :
        cluster_tot[cluster] = 0
        for nn in nns:
            if nns_by_cluster[cluster].has_key(nn):
                cluster_tot[cluster] += nns_by_cluster[cluster][nn]
                tot += nns_by_cluster[cluster][nn]
                if nns_tot.has_key(nn) :
                    nns_tot[nn] += nns_by_cluster[cluster][nn]
                else :
                    nns_tot[nn] = nns_by_cluster[cluster][nn]

    nns_by_cluster_wished = {}
    for cluster in nns_by_cluster.keys() :
        nns_by_cluster_wished[cluster] = {}
        for nn in nns:
            nns_by_cluster_wished[cluster][nn] = (float(nns_tot[nn])*float(cluster_tot[cluster]))/float(tot)

    nns_by_cluster_chival = {}
    for cluster in nns_by_cluster.keys() :
        nns_by_cluster_chival[cluster] = {}
        for nn in nns:
            if nns_by_cluster[cluster].has_key(nn) and int(nns_by_cluster_wished[cluster][nn]) != 0:
                o1 = float(nns_by_cluster[cluster][nn])
                o2 = float(nns_by_cluster_wished[cluster][nn])
                nns_by_cluster_chival[cluster][nn] = ((o1-o2)*(o1-o2))/o2
            else :
                nns_by_cluster_chival[cluster][nn] = 0

    ddl = int(math.pow(4, nb_nns)) - 1
    chi2_val = CHI2_TABLE[str(ddl)][p_value]
        
    chi2_cluster_tot = {}
    to_correct = [None, None]
    for cluster in nns_by_cluster_chival.keys() :
        chi2_cluster_tot[cluster] = 0
        for nn in nns:
            if nns_by_cluster_chival[cluster].has_key(nn):
                chi2_cluster_tot[cluster] += nns_by_cluster_chival[cluster][nn]
        # There is a problem in this cluster
        if chi2_cluster_tot[cluster] > chi2_val :
            # Then figure out which chi2_nn has to be modified
            nn_to_avoid = []
            
            # Until a candidate has been found
            while True:
                # First check if all nns were tested
                if len(nns) == len(nn_to_avoid):
                    log.write("## Impossible to correct this cluster " + cluster + " (chi2=" + str(chi2_cluster_tot[cluster]) + ")\n")
                    log.write("Wished values = " + str(nns_by_cluster_wished[cluster]) + "\n")
                    log.write("Observed values = " + str(nns_by_cluster[cluster]) + "\n")
                    break
                # First find which nn has the max value of chi2
                max_chi2 = 0
                max_chi2_nn = None
                for nn in nns:
                    if nn not in nn_to_avoid:
                        if nns_by_cluster_chival[cluster].has_key(nn):
                            if nns_by_cluster_chival[cluster][nn] > max_chi2:
                                max_chi2_nn = nn
                                max_chi2 = nns_by_cluster_chival[cluster][nn]
                # If the nn with higher chi2 has a number of nn wished > to the number of nn observed, change the candidate
                if int(nns_by_cluster_wished[cluster][max_chi2_nn]) >= int(nns_by_cluster[cluster][max_chi2_nn]) :
                    nn_to_avoid.append(max_chi2_nn)
                else :
                    to_correct = [cluster, max_chi2_nn]
                    log.write("## Correcting cluster " + cluster + " (chi2=" + str(chi2_cluster_tot[cluster]) + ", nn=" + max_chi2_nn + ")\n")
                    log.write(str(nns_by_cluster_chival[cluster])+"\n")
                    break
            break
    return to_correct
            

def create_accnos_file(cluster_correction, seq_nns, options):
    """
    Create a accnos file with sequence ids to delete
      @param nns_by_cluster : {"cluster": {"AA" : n, ...}, ...}
      @param nns            : ["AA", ...]
      @param nb_nns         : Number of NNs used
      @param p_value        : the p_value to use for the chi2 test
      @return               : ["cluster_name", "NN_value"] to correct [None, None] if no chi2 error found
    """
    seqs_by_cluster = get_seqs_by_cluster(seq_nns, options.fasta_file, options.names_file)
    ids_to_delete = []
    for cluster in cluster_correction.keys():
        for nns_to_del in cluster_correction[cluster].keys():
            nb = cluster_correction[cluster][nns_to_del]
            for i in range(nb):
                shorter = sys.maxint
                shorter_id = None
                for seq_to_test in seqs_by_cluster[cluster][nns_to_del]:
                    if len(seq_to_test) < shorter and seq_to_test.id not in ids_to_delete:
                        shorter = len(seq_to_test)
                        shorter_id = seq_to_test.id
                ids_to_delete.append(shorter_id)
    handle = open(os.path.join(options.output_folder, os.path.basename(options.fasta_file)+".accnos"), "w")
    for id_to_delete in ids_to_delete:
        handle.write(id_to_delete+"\n")
    handle.close()    


def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def nnpyrocleaner_depts_error():
    """
    Return the list of dependencies missing to run
    """
    error = ""
    if which("cross_match") == None and options.clean_pairends != None:
        error += " - cross_match\n"
    if error != "":
        error = "Cannot execute %prog, following dependencies are missing :\n" + error + "Please install them before to run!"
    return error
    
if __name__ == '__main__':
    
    parser = OptionParser(usage="Usage: %prog -in fasta_file -name names_file")
    
    igroup = OptionGroup(parser, "Input options","")
    igroup.add_option("-i", "--in", dest="fasta_file",    help="The fasta file", metavar="FILE")
    igroup.add_option("-n", "--names", dest="names_file", help="The names file", metavar="FILE")
    igroup.add_option("-f", "--forward", dest="forward",  help="The forward primer used", type="string")
    igroup.add_option("-r", "--reverse", dest="reverse",  help="The reverse primer used", type="string")
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output options","")
    ogroup.add_option("-o", "--out", dest="output_folder", help="The output folder where datas are stored")
    ogroup.add_option("-g", "--log", dest="log_file",      help="The log file name default is NNpyrocleaner.log", type = "string", default="NNpyrocleaner.log")
    parser.add_option_group(ogroup)

    tpgroup = OptionGroup(parser, "Computing parameters","")
    tpgroup.add_option("-u", "--full-resolution", help="If sets uses NNs from forward and reverse",            dest="full_resolution", action="store_true", default=False)
    tpgroup.add_option("-p", "--p-value",         help="The p value to use by the chi2 test [0.01|0.05|0.10]", dest="p_value",         type="string",       default="0.05")
    tpgroup.add_option("-v", "--only-reverse",    help="If sets uses NNs only from reverse strand",            dest="only_reverse",    action="store_true", default=False)
    tpgroup.add_option("-x", "--nb-nns",          help="The number of nns used (default=2)",                   dest="nb_nns",          type="int",          default=2)
    tpgroup.add_option("-s", "--rvrs-minmatch",   help="Min number of match for the reverse primer",           dest="rvrs_minmatch",   type="int",          default=12)
    tpgroup.add_option("-b", "--fwd-minmatch",    help="Min number of match for the forward primer",           dest="fwd_minmatch",    type="int",          default=12)
    parser.add_option_group(tpgroup)

    (options, args) = parser.parse_args()

    if not nnpyrocleaner_depts_error():

        if options.fasta_file == None and options.names_file == None :
            parser.print_help()
            sys.exit(1)
        else :
            
            global log
            log = open(os.path.join(options.output_folder, options.log_file), "w")
            log.write("## Start processing (" + str(datetime.datetime.now()) + ")\n")
            
            [seq_nns, nns] = get_nns(options)
            nns_by_cluster = sort_nns_by_cluster(seq_nns, options.names_file)
            
            cluster_correction = {}
            while True:
                [error_cluster, error_nn] = chi2(nns_by_cluster, nns, options.nb_nns, options.p_value)
                # If there is an error
                if error_cluster and error_nn:
                    nns_by_cluster[error_cluster][error_nn] -= 1
                    if cluster_correction.has_key(error_cluster) :
                        if cluster_correction[error_cluster].has_key(error_nn) :
                            cluster_correction[error_cluster][error_nn] += 1
                        else :
                            cluster_correction[error_cluster][error_nn] = 1
                    else :
                        cluster_correction[error_cluster] = {}
                        cluster_correction[error_cluster][error_nn] = 1
                else :
                    break
            
            create_accnos_file(cluster_correction, seq_nns, options)

            log.write("## Ended with code 0 (" + str(datetime.datetime.now()) + ")\n")
            log.close()
            sys.exit(0)
    else : 
        print nnpyrocleaner_depts_error()
        sys.exit(1)


